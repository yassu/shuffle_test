#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools

setuptools.setup(
    name='shuffle_test',
    version='0.1',
    packages=setuptools.find_packages(),
)
