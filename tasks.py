#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List, Optional

import term
from invoke import task


def print_title(title: str):
    term.writeLine("running " + title, term.green)


@task
def format(ctx):
    ctx.run("isort -rc .")
    ctx.run('yapf --in-place -r .')


@task
def test(ctx, with_time=False):
    """ テストする """
    def pytest_args(with_time: bool, verbose: bool, cov_report: Optional[str]) -> \
            List[str]:
        return [
            "pytest", "-vv" if verbose else "", "--durations 5" if with_time else "",
            "--cov=shuffle_test/", f"--cov-report={cov_report}" if cov_report else "", "-s",
            "tests"
        ]

    print_title("pytest")
    ctx.run(" ".join(pytest_args(with_time=with_time, verbose=True, cov_report=None)))
    ctx.run(
        " ".join(pytest_args(with_time=with_time, verbose=True, cov_report="html")) +
        " > /dev/null")

    print_title("flake8")
    ctx.run("flake8")

    print_title("isort")
    ctx.run("isort --check-only -rc shuffle_test/ tests/")
