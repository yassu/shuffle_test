#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import sample as _random_sample

from shuffle_test.entities import Deck


def get_random_deck(size: int) -> Deck:
    return Deck(_random_sample(list(range(size)), size))
