#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shuffle_test.entities import Deck


@pytest.mark.parametrize(
    'deck, result', [
        (Deck([0, 1, 2, 3, 4, 5, 6]), 0), (Deck([0, 2, 1, 3, 4, 6, 5]), 3),
        (Deck([3, 1, 0, 2, 4, 5, 6]), 3)
    ])
def test_deck_score(deck, result):
    assert deck.score == result
