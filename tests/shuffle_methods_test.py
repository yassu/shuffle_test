#!/usr/bin/env python
# -*- coding: utf-8 -*-
from shuffle_test.shuffle_methods import DealShuffleMethod, FallowShuffleMethod, HinduShuffleMethod
from tests.test_util import get_random_deck


def test_HinduShuffleMethod_shuffle():
    # shuffleしてもdeckの値は変わらないこと
    shuffle_method = HinduShuffleMethod()
    old_deck = get_random_deck(10)
    deck = old_deck[:]
    shuffle_method.shuffle(old_deck)

    assert deck == old_deck


def test_HinduShuffleMethod_shuffle2():
    # shuffleしてもdeckの値は集合として変わらないこと
    shuffle_method = HinduShuffleMethod()
    old_deck = get_random_deck(10)
    deck = shuffle_method.shuffle(old_deck)
    assert set(deck) == set(old_deck)
    assert len(deck) == len(old_deck)


def test_DealShuffleMethod_shuffle():
    """ シャッフルしてもdeckの値は変わらないこと """
    shuffle_method = DealShuffleMethod(7)
    old_deck = get_random_deck(10)
    deck = old_deck[:]
    shuffle_method.shuffle(old_deck)

    assert deck == old_deck


def test_DealShuffleMethod_shuffle2():
    """ シャッフルしても それぞれの値は変わらないこと """
    shuffle_method = DealShuffleMethod(7)
    old_deck = get_random_deck(10)
    new_deck = shuffle_method.shuffle(old_deck)

    assert set(old_deck) == set(new_deck)


def test_DealShuffleMethod_shuffle3():
    """ 7個の山に分けたとき old_deck[0], old_deck[7] 及び old_deck[1], old_deck[8] が隣り合っていること """
    shuffle_method = DealShuffleMethod(7)
    old_deck = get_random_deck(10)
    zero_value = old_deck[0]
    seven_value = old_deck[7]
    one_value = old_deck[1]
    eight_value = old_deck[8]

    deck = shuffle_method.shuffle(old_deck)
    zero_index = deck.index(zero_value)
    seven_index = deck.index(seven_value)
    one_index = deck.index(one_value)
    eight_index = deck.index(eight_value)

    assert seven_index - zero_index == 1
    assert eight_index - one_index == 1


def test_FallowShuffleMethod_shuffle():
    """ シャッフルしてもdeckの値は変わらないこと """
    shuffle_method = FallowShuffleMethod(3)
    old_deck = get_random_deck(20)
    deck = old_deck[:]
    shuffle_method.shuffle(old_deck)

    assert deck == old_deck


def test_FallowShuffleMethod_shuffle2():
    """ シャッフルしても それぞれの値は変わらないこと """
    shuffle_method = FallowShuffleMethod(7)
    old_deck = get_random_deck(10)
    new_deck = shuffle_method.shuffle(old_deck)

    assert set(old_deck) == set(new_deck)
