#!/usr/bin/env python
# -*- coding: utf-8 -*-

from shuffle_test.utility import popleft
from tests.test_util import get_random_deck


def test_popleft():
    deck = get_random_deck(10)

    first = deck[0]
    size = len(deck)

    assert popleft(deck) == first
    assert len(deck) + 1 == size
