#!/usr/bin/env python
# -*- coding: utf-8 -*-

from shuffle_test.entities import Deck


def popleft(deck: Deck) -> int:
    card = deck[0]
    del (deck[0])
    return card
