#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import sample as _random_sample
import statistics

from entities import Deck
from shuffle_methods import DealShuffleMethod, FallowShuffleMethod, HinduShuffleMethod


def get_shuffle_methods(number_of_hindu_shaffle=0, number_of_deal=0, number_of_fallow=0):
    return (
        [FallowShuffleMethod(max_number_of_between_cards=3)] * number_of_fallow +
        [DealShuffleMethod(number_of_mountain=7)] * number_of_deal +
        [HinduShuffleMethod()] * number_of_hindu_shaffle)


def get_init_deck(size: int) -> Deck:
    return Deck(list(range(size)))


def get_random_deck(size: int) -> Deck:
    return Deck(_random_sample(list(range(size)), size))


def compute_one(size, shuffle_methods):
    deck = get_init_deck(size)
    deck = deck.shuffle(shuffle_methods)
    # print(deck)
    return deck.score


DECK_SIZE = 40
NUMBER_OF_SAMPLES = 10 ** 4
NUMBER_OF_HINDU_SHAFFLE = 15
NUMBER_OF_DEAL = 0
NUMBER_OF_FALLOW = 0


def main() -> None:
    shuffle_methods = get_shuffle_methods(
        number_of_hindu_shaffle=NUMBER_OF_HINDU_SHAFFLE,
        number_of_deal=NUMBER_OF_DEAL,
        number_of_fallow=NUMBER_OF_FALLOW)
    # print(shuffle_methods)
    scores = [compute_one(DECK_SIZE, shuffle_methods) for _ in range(NUMBER_OF_SAMPLES)]
    # print('score', scores)
    print('mean', statistics.mean(scores))
    print('median', statistics.median(scores))


if __name__ == '__main__':
    main()
