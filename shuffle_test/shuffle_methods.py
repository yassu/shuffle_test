#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dataclasses import dataclass
from random import randint

from shuffle_test.entities import Deck
from shuffle_test.utility import popleft


class ShuffleMethod:

    def shuffle(self, deck: Deck) -> Deck:
        ...


class HinduShuffleMethod(ShuffleMethod):

    def shuffle(self, deck: Deck) -> Deck:
        deck = deck[:]
        cards = []
        while len(deck) != 0:
            n = randint(0, len(deck) - 1)
            cards = cards + deck[n:]
            del(deck[n:])
        return Deck(cards)


@dataclass
class DealShuffleMethod(ShuffleMethod):
    number_of_mountain: int

    def shuffle(self, deck: Deck) -> Deck:
        a = [[] for _ in range(self.number_of_mountain)]
        for i, card in enumerate(deck):
            a[i % self.number_of_mountain].append(card)

        return sum(a, [])


@dataclass
class FallowShuffleMethod(ShuffleMethod):
    max_number_of_between_cards: int

    def shuffle(self, deck: Deck) -> Deck:
        deck1 = deck[:len(deck) // 2]
        deck2 = deck[len(deck) // 2:]
        new_cards = []
        count = randint(0, 1)

        while len(deck1) > 0 or len(deck2) > 0:
            number_of_cards = randint(1, self.max_number_of_between_cards)
            if count % 2 == 0:
                new_cards.extend([popleft(deck1) for i in range(min(len(deck1), number_of_cards))])
            else:
                new_cards.extend([popleft(deck2) for i in range(min(len(deck2), number_of_cards))])
            count += 1

        return Deck(new_cards)
