#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Deck(list):

    def shuffle(self, shuffle_methods) -> "Deck":
        deck = self
        for shuffle_method in shuffle_methods:
            deck = shuffle_method.shuffle(deck)
        return deck

    @property
    def score(self) -> int:
        s = 0
        for i in range(len(self) - 1):
            if abs(self[i] - self[i + 1]) > 1:
                s += 1

        return s
